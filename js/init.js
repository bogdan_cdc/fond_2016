$(window).load(function(){
	$('.slider').bxSlider({
		mode: 'fade',
		controls: false
	});
	$('.tabs_cont').bxSlider({
		mode: 'fade',
		pagerCustom: '.tabs',
		controls: false
	});
	$('.quotes_slide').bxSlider({
		mode: 'fade',
		controls: false
	});

	function bxMe(bx_el, custom_pg) {
		$(bx_el).bxSlider({
			mode: 'fade',
			pagerCustom: custom_pg,
			controls: false
		});

		console.log(bx_el, typeof custom_pg);
	}

	$('.custom_pg').each(function(){
		var bx_el = $(this),
			custom_pg = bx_el.parents('.n_parent').find('.custom_pg_wrap').data('class');
		console.log(bx_el, custom_pg);
		bxMe(bx_el, custom_pg);
	});

	function scrllTo(target, get_offset) {
		$(window).scrollTo(target,800,{
			offset: get_offset
		})
	};
	$('.scrll').click(function () {
		var offset = parseInt($(this).data('offset')),
			target = $(this).attr('href');
		scrllTo(target, offset);
		event.preventDefault();
	});

});