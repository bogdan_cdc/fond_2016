var gulp = require('gulp');

var gulpif = require('gulp-if');
var sass = require('gulp-sass');
//var sprite = require('sprity');


var sourcemaps = require('gulp-sourcemaps');

var browserSync = require('browser-sync');
var reload      = browserSync.reload;

var paths = {
  html: ['index.html'],
  css: ['./css/*.scss'],
  sprite_images: ['./img/*.png']
};



var gulpif = require('gulp-if');
//var sprite = require('sprity').stream;


/*gulp.task('sprites', function () {
  return sprity.src({
    src: paths.sprite_images,
    style: './_sprite.css',
    processor: 'sass', // make sure you have installed sprity-sass 
  })
  .pipe(gulpif('*.png', gulp.dest('./images/'), gulp.dest('./')))
});*/




//  css


gulp.task('prepros', function(){
  return gulp.src(paths.css)
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'expanded'
  }).on('error', sass.logError))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('./css'))
  .pipe(reload({stream:true}));
});



/*gulp.task('css', function () {
    return gulp.src('./*.css')
        .pipe(postcss([ autoprefixer({ browsers: ['ie 6-8, opera'] }) ]))
        .pipe(gulp.dest('./'));
});*/


gulp.slurped = false;

gulp.task('watcher',function(){

  if(!gulp.slurped){
      gulp.watch("gulpfile.js", ["default"]);
      //gulp.watch(paths.sprite_images, ['sprites']);
      //gulp.watch(paths.css, ['css']);
      gulp.watch(paths.css, ['prepros']);
      //gulp.watch(paths.html, ['html']);
      gulp.slurped = true;
  }
});

gulp.task('default', ['watcher'/*, 'browserSync'*/]);